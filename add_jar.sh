#! /bin/sh

usage () {
    echo "Usage:\n  add_jar.sh groupId artifactId version libFile repositoryPath"
    echo "PS: Maven has to be installed in your system to proceed."
    exit 1;
}

if [ $# -ne 5 ]; then
    usage
fi

if [ $1 = "--help" ]; then
    usage
fi

# GROUP_ID=com.steam
# ARTIFACT_ID=jaxis
# VERSION=1
# LIB_FILE=./jaxis.jar
# REPOSITORY=/home/warmouse/work/reposteam/maven

GROUP_ID="$1"
ARTIFACT_ID="$2"
VERSION="$3"
LIB_FILE="$4"
REPOSITORY="$5"

# установка jar без зависимостей
#mvn install:install-file -DgroupId="$GROUP_ID" -DartifactId="$ARTIFACT_ID" -Dversion="$VERSION" -Dfile="$LIB_FILE" -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath="$REPOSITORY" -DcreateChecksum=true

# Установка с подтягиванием всех зависимых jar
#mvn install:install-file -DpomFile=datatransport/pom.xml -Dfile=datatransport/target/dist/datatransport-1.0.3.jar -DlocalRepositoryPath=/home/warmouse/work/repos/reposteam/maven -DcreateChecksum=true
