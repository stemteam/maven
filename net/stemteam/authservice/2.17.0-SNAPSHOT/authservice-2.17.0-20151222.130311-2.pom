<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.stemteam</groupId>
    <artifactId>authservice</artifactId>
    <version>2.17.0-SNAPSHOT</version>
    <packaging>jar</packaging>
    <properties>
        <project.dest.filename>authservice.jar</project.dest.filename>
    </properties>

    <build>
        <!-- формат файла сборки -->
        <finalName>${project.artifactId}-${project.version}</finalName>
        <plugins>
            <!-- Плагин для настроек компилятора -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>7</source>
                    <target>7</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <!-- настройка манифеста -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <classpathPrefix>lib</classpathPrefix>
                            <mainClass></mainClass>
                        </manifest>
                        <manifestEntries>
                            <Version>${project.version}</Version>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>
            <!-- настройки зависиместей + перенос -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy_jar</id>
                        <phase>package</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>${project.groupId}</groupId>
                                    <artifactId>${project.artifactId}</artifactId>
                                    <version>${project.version}</version>
                                    <type>${project.packaging}</type>
                                    <outputDirectory>${project.build.directory}/dist</outputDirectory>
                                    <destFileName>${project.dest.filename}</destFileName>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- Перенос файла настроек properties.xml -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.6</version>
                <executions>
                    <execution>
                        <id>copy_config</id>
                        <phase>package</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/dist</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>src/main/resources/config</directory>
                                    <filtering>false</filtering>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- запуск генерации wiki-документации при сборке -->
            <plugin>
              <groupId>org.codehaus.mojo</groupId>
              <artifactId>exec-maven-plugin</artifactId>
              <version>1.2</version>
              <executions>
                <execution>
                  <id>build-dump</id>
                  <phase>process-classes</phase>
                  <goals>
                    <goal>java</goal>
                  </goals>
                  <configuration>
                    <mainClass>net.stemteam.authservice.doc.WikiDoc</mainClass>
                    <arguments>
                      <!-- имя файла для генерации докувики -->
                      <argument>${project.build.directory}/dist/wiki/auth.txt</argument>
                      <argument>===== Описание API модуля Авторизации =====</argument>
                    </arguments>
                  </configuration>
                </execution>
              </executions>
            </plugin>
        </plugins>
        <extensions>
          <extension>
            <groupId>ar.com.synergian</groupId>
            <artifactId>wagon-git</artifactId>
            <version>0.2.5</version>
          </extension>
        </extensions>
    </build>

    <distributionManagement>
      <repository>
        <id>release.git.maven</id>
        <url>git:master://git@bitbucket.org:stemteam/maven.git</url>
      </repository>

      <snapshotRepository>
        <id>snapshot.git.maven</id>
        <url>git:master://git@bitbucket.org:stemteam/maven.git</url>
      </snapshotRepository>
    </distributionManagement>

    <dependencies>
        <dependency>
            <groupId>net.stemteam</groupId>
            <artifactId>jaxis</artifactId>
            <version>3.11.0</version>
        </dependency>
    </dependencies>

    <repositories>
        <repository>
            <id>stemteamrepo</id>
            <url>https://api.bitbucket.org/1.0/repositories/stemteam/maven/raw/master</url>
            <releases>
              <enabled>true</enabled>
            </releases>
            <snapshots>
              <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

    <pluginRepositories>
      <pluginRepository>
        <id>synergian-repo</id>
        <url>https://raw.github.com/synergian/wagon-git/releases</url>
      </pluginRepository>
    </pluginRepositories>
</project>
